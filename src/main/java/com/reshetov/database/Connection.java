package com.reshetov.database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connection {
    private static Logger log = Logger.getLogger(Connection.class.getName());

    static java.sql.Connection connection;
    static PreparedStatement pstm;
    static Statement stm;
    static ResultSet rs;


    public static void connect() {

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:main.db");

        } catch (ClassNotFoundException | SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            new RuntimeException("Невозможно подключиться к базе данных");
        }
    }

    public static void disconnect() {
        try {
            if (!rs.isClosed()) {
                rs.close();
            }

            if (!stm.isClosed()) {
                stm.close();
            }
            if (connection != null) {
                connection.close();
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }
}
