package com.reshetov.database;

import com.reshetov.entities.Employee;
import com.reshetov.entities.Product;
import com.reshetov.entities.Student;
import com.reshetov.entities.Task;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DAO {
    private static Logger log = Logger.getLogger(DAO.class.getName());

    Connection conn = new Connection();
    ArrayList<String> methodsArr = new ArrayList<String>() {{
        add("/loadAllItems");
        add("/searchBySpec(spec_name)");
        add("/countAvgMarkOnSpec(spec_name)");
        add("/addStudent(name,avg_mark,spec_name,summary)");
        add("/addStudentViaTxt(filePath)");
        add("/importStudentsToJson");
        add("/exit");
    }};
    List<Employee> employees = new ArrayList<>();
    List<Student> students = new ArrayList<>();
    List<Product> products = new ArrayList<>();
    List<Task> tasks = new ArrayList<>();
    int markSum, countStudents;

    public List<Student> getStudents() {
        return students;
    }

    public void help() {
        log.entering("DAO", "help");
        log.info("TRACE: " + "количество методов " + methodsArr.size());
        System.out.println("Программа содержит следущие методы: ");
        for (int i = 0; i < methodsArr.size(); i++) {

            System.out.println(methodsArr.get(i));

        }
        log.exiting("DAO", "help");
    }

    public boolean checkPrintAllItems() {
        return methodsArr.contains("/printAllItems");
    }


    public void exit() {
        log.entering("DAO", "exit");
        System.exit(0);
        log.exiting("DAO", "exit");
    }


    public void addStudent(String name, double avarage_mark, String specialization_name, String summary) {
        log.entering("DAO", "addStudent");
        try {
            conn.pstm = conn.connection.prepareStatement("Insert into Specializations (name, summary) values (?, ?) ");
            conn.pstm.setString(1, specialization_name);
            conn.pstm.setString(2, summary);
            conn.pstm.addBatch();
            conn.pstm.executeBatch();

            conn.pstm = conn.connection.prepareStatement("Insert into Students (name, avarage_mark, specialization_id) values ( ?, ?, (Select id from Specializations where Specializations.name=?)) ");

            conn.pstm.setString(1, name);
            conn.pstm.setDouble(2, avarage_mark);
            conn.pstm.setString(3, specialization_name);
            conn.pstm.addBatch();
            conn.pstm.executeBatch();
            System.out.println("Студент " + name + " успешно добавлен!");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }

    public void addStudentViaTxt(String name, double avarage_mark, int specialization_id) {
        log.entering("DAO", "addStudentViaTxt");

        try {
            conn.pstm = conn.connection.prepareStatement("Insert into Students (name, avarage_mark, specialization_id) values ( ?, ?, ?) ");

            conn.pstm.setString(1, name);
            conn.pstm.setDouble(2, avarage_mark);
            conn.pstm.setInt(3, specialization_id);
            conn.pstm.addBatch();
            conn.pstm.executeBatch();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
        System.out.println("Студент " + name + " успешно добавлен!");
    }


    public void connectToDB() {
        conn.connect();
    }

    public void closeConnectToDB() {
        conn.disconnect();
    }

    public void loadAllItems() {
        System.out.println("Соединение с базой данных...");
        try {
            log.entering("DAO", "loadAllItems");
            conn.stm = conn.connection.createStatement();
            System.out.println("Загружаем студентов...");
            conn.rs = conn.stm.executeQuery("Select Students.id, Students.name, Students.avarage_mark, Specializations.name as specializationname, Specializations.summary from Students, Specializations where Students.specialization_id=Specializations.id");
            while (conn.rs.next()) {
                students.add(new Student(conn.rs.getInt("id"), conn.rs.getString("name"), conn.rs.getString("specializationname"), conn.rs.getString("summary"), conn.rs.getDouble("avarage_mark")));
            }
            System.out.println("Студенты загружены!");
            System.out.println("Загружаем работников...");
            conn.rs = conn.stm.executeQuery("Select * from Employees, Extrainfos where extrainfo_id=Extrainfos.id_extrainfo");
            while (conn.rs.next()) {
                employees.add(new Employee(conn.rs.getInt("id"), conn.rs.getInt("age"), conn.rs.getString("name"), conn.rs.getString("position"), conn.rs.getString("number"), conn.rs.getString("address"), conn.rs.getInt("salary")));
            }
            System.out.println("Работники загружены!");
            System.out.println("Загружаем товары...");
            conn.rs = conn.stm.executeQuery("Select * from Products, Categories where category_id=Categories.id_category");
            while (conn.rs.next()) {
                products.add(new Product(conn.rs.getInt("id"), conn.rs.getString("category_name"), conn.rs.getString("name"), conn.rs.getString("summary"), conn.rs.getDouble("price"), conn.rs.getString("category_summary")));
            }
            System.out.println("Товары загружены!");
            System.out.println("Загружаем задачи...");
            conn.rs = conn.stm.executeQuery("Select * from Tasks, Statuses where status_id=Statuses.id");
            while (conn.rs.next()) {
                tasks.add(new Task(conn.rs.getInt("id"), conn.rs.getInt("priority"), conn.rs.getString("name"), conn.rs.getString("summary"), conn.rs.getString("status_name")));
            }
            System.out.println("Задачи загружены!");
            methodsArr.add("/printAllItems");
            System.out.println("Для того чтобы просмотреть все объекты введите '/printAllItems'");
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }


    public void searchStudentsBySpecialization(String spec_name) {
        try {
            log.entering("DAO", "searchStudentBySpecialization");
            conn.pstm = conn.connection.prepareStatement("Select * from Students, Specializations where specialization_id=Specializations.id and Specializations.name = ?");
            conn.pstm.setString(1, spec_name);

            conn.rs = conn.pstm.executeQuery();

            System.out.println("Студенты со специализацией " + spec_name + ":");
            while (conn.rs.next()) {
                System.out.println("Имя " + conn.rs.getString("name") + ", Средний балл " + conn.rs.getDouble("avarage_mark"));
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }

    public void loadAllStudents() {
        try {
            log.entering("DAO", "loadAllStudents");
            conn.stm = conn.connection.createStatement();
            conn.rs = conn.stm.executeQuery("Select Students.id, Students.name, Students.avarage_mark, Specializations.name as specializationname, Specializations.summary from Students, Specializations where Students.specialization_id=Specializations.id");
            while (conn.rs.next()) {
                students.add(new Student(conn.rs.getInt("id"), conn.rs.getString("name"), conn.rs.getString("specializationname"), conn.rs.getString("summary"), conn.rs.getDouble("avarage_mark")));
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }

    public void countAvarageMarkForSpecialization(String spec_name) {
        try {
            conn.pstm = conn.connection.prepareStatement("Select * from Students, Specializations where specialization_id=Specializations.id and Specializations.name = ?");
            conn.pstm.setString(1, spec_name);

            conn.rs = conn.pstm.executeQuery();

            while (conn.rs.next()) {
                markSum += conn.rs.getInt("avarage_mark");
                countStudents++;
            }

            System.out.println("Средний балл студентов на специальности " + spec_name + " = " + markSum / countStudents);
        } catch (SQLException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }

    public void printAllItems() {
        log.entering("DAO", "printAllItems");
        System.out.println("Список студентов:");
        for (int i = 0; i < students.size(); i++) {
            System.out.println(students.get(i));
        }
        System.out.println("Список работников: ");
        for (int i = 0; i < employees.size(); i++) {
            System.out.println(employees.get(i));
        }
        System.out.println("Список задач: ");
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println(tasks.get(i));
        }
        System.out.println("Список товаров: ");
        for (int i = 0; i < products.size(); i++) {
            System.out.println(products.get(i));
        }
    }
}
