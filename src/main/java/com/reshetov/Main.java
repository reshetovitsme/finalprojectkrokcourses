package com.reshetov;

import com.reshetov.database.DAO;
import com.reshetov.importAndExport.Export;
import com.reshetov.importAndExport.Import;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.LogManager;

public class Main {

    static DAO queries = new DAO();
    static Export exportTxt = new Export();
    static Import importJson = new Import();
    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        try {
            FileInputStream fileInputStream = new FileInputStream(new File("D:\\WorkSpace\\study\\KROK\\FinalProject\\src\\main\\resources\\logging.properties"));
        LogManager.getLogManager().readConfiguration(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String argument;

        System.out.println("Добро пожаловать в финальный курсовой проект");
        System.out.println("Чтобы получить список команд введите '/help' или");
        queries.connectToDB();
        while (true) {
            System.out.println("Введите команду и параметры: ");
            String in = scanner.nextLine();
            String[] cmd = in.split("\\(");
            switch (cmd[0]) {
                case "/importStudentsToJson":
                    importJson.importStudentsToJson();
                    break;
                case "/loadAllItems":
                    queries.loadAllItems();
                    break;
                case "/addStudentViaTxt":
                    argument = cmd[1].substring(0, cmd[1].length() - 1);
                    exportTxt.addStudentViaTxt(argument);
                    break;
                case "/help":
                    queries.help();
                    break;
                case "/searchBySpec":
                    argument = cmd[1].substring(0, cmd[1].length() - 1);
                    queries.searchStudentsBySpecialization(argument);
                    break;
                case "/countAvgMarkOnSpec":
                    argument = cmd[1].substring(0, cmd[1].length() - 1);
                    queries.countAvarageMarkForSpecialization(argument);
                    break;
                case "/addStudent":
                    argument = cmd[1].substring(0, cmd[1].length() - 1);
                    String[]arguments = argument.split(",");
                    queries.addStudent(arguments[0], Double.valueOf(arguments[1]), arguments[2], arguments[3]);
                    break;
                case "/printAllItems":
                    if(queries.checkPrintAllItems())
                    {
                        queries.printAllItems();
                        break;
                    }
                    else {
                        System.out.println("Перед запуском этой команды нужно выполнить /loadAllItems");
                        System.out.println("Сейчас она запустится автоматически, а затем выполнится текущая");
                        queries.loadAllItems();
                        queries.printAllItems();
                        break;
                    }
                case "/exit":
                    queries.closeConnectToDB();
                    queries.exit();
                default:
                    System.out.println("Команды не существует! Для получения списка команда введите '/help'.");
                    break;
            }
        }
    }
}
