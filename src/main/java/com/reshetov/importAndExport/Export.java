package com.reshetov.importAndExport;

import com.reshetov.database.DAO;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Export {
    private static Logger log = Logger.getLogger(Export.class.getName());

    DAO dao = new DAO();

    public void addStudentViaTxt(String path) {
        log.entering("Export", "addStudentViaTxt");

        String name = null;
        double avarage_mark = 0;
        int specialization_id = 0;

        try {
            FileInputStream fstream = new FileInputStream(path);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));

            String strLine;
            int counterBefore = 0;
            int counterAfter = 0;
            while ((strLine = reader.readLine()) != null) {
                if (strLine.equals("Student")) {
                    counterBefore++;
                }
                else {
                    String[] argumetns = strLine.split("=");
                    if (argumetns[0].equals("name")) {
                        name = argumetns[1];
                        counterAfter++;
                    }
                    if (argumetns[0].equals("avarage_mark")) {
                        avarage_mark = Double.valueOf(argumetns[1]);
                        counterAfter++;
                    }
                    if (argumetns[0].equals("specialization_id")) {
                        specialization_id = Integer.valueOf(argumetns[1]);
                        counterAfter++;
                    }
                }
                if (counterBefore * 3 == counterAfter && counterBefore!=0) {
                    dao.addStudentViaTxt(name, avarage_mark, specialization_id);
                    counterAfter=0;
                    counterBefore=0;
                }
            }
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }
}
