package com.reshetov.importAndExport;

import com.reshetov.database.DAO;
import com.reshetov.entities.Student;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Import {
    DAO dao = new DAO();
    private static Logger log = Logger.getLogger(Import.class.getName());
    private static final String FILENAME = "D:\\WorkSpace\\study\\KROK\\FinalProject\\src\\main\\resources\\Students.json";

    public void importStudentsToJson() {
        log.entering("Import", "importStudentsToJson");
        List<Student> studentsFromDB = new ArrayList<>();

        JSONArray students = new JSONArray();
        dao.loadAllStudents();
        studentsFromDB = dao.getStudents();
        for (int i = 0; i < studentsFromDB.size(); i++) {
            JSONObject properies = new JSONObject();
            JSONObject student = new JSONObject();
            properies.put("id", studentsFromDB.get(i).getId());
            properies.put("name", studentsFromDB.get(i).getName());
            properies.put("avarage_mark", studentsFromDB.get(i).getAvarage_mark());
            properies.put("spec_name", studentsFromDB.get(i).getSpecialization_name());
            properies.put("spec_summary", studentsFromDB.get(i).getSummary());
            student.put("Student", properies);
            students.add(student);
        }


        try {
            FileWriter writer = new FileWriter(FILENAME);
            writer.write(students.toJSONString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
        }
    }
}
