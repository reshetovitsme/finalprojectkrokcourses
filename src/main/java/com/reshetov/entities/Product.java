package com.reshetov.entities;

public class Product {
    int id;
    String category, name, summary, category_summary;
    double price;

    public Product(int id, String category, String name, String summary, double price, String category_summary) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.summary = summary;
        this.category_summary = category_summary;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
