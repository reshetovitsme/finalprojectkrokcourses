package com.reshetov.entities;

public class Student {

    private int id;
    private String name, specialization_name, summary;
    private double avarage_mark;

    public Student(int id, String name, String specialization_name, String summary, double avarage_mark) {
        this.id = id;
        this.name = name;
        this.specialization_name = specialization_name;
        this.summary = summary;
        this.avarage_mark = avarage_mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization_name() {
        return specialization_name;
    }

    public void setSpecialization_name(String specialization_name) {
        this.specialization_name = specialization_name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getAvarage_mark() {
        return avarage_mark;
    }

    public void setAvarage_mark(double avarage_mark) {
        this.avarage_mark = avarage_mark;
    }
}
