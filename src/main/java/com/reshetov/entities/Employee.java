package com.reshetov.entities;

public class Employee {

    private int id_employee, age;
    private String name, position, number, address;
    private double salary;

    public Employee(int id_employee, int age, String name, String position, String number, String address, double salary) {
        this.id_employee = id_employee;
        this.age = age;
        this.name = name;
        this.position = position;
        this.number = number;
        this.address = address;
        this.salary = salary;
    }

    public int getId_employee() {
        return id_employee;
    }

    public void setId_employee(int id_employee) {
        this.id_employee = id_employee;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
